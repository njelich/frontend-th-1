# Take-home Criteria

## Must Have

- The application must start
- The application must be a single-page application (SPA)
- The application must be divided into components
- The file structure must be consistent and easy to follow
- All use cases should be covered with only minor issues
- The application must include meaningful tests (meaningful tests validate logic or component behavior; superficial tests like snapshot tests, or tests that just validate if a component renders are not accepted)

## Nice to Have

- The application is configurable by environment variables
- The application has a development and production environment
- The application supports code splitting
- The application is responsive
- A component library is created
- The application has good naming
- The application computes values
- Cross-browser techniques are implemented
- Styles are stored separately

## Negative Points

- No componentization
- No modularization
- Inline styles
- No control over re-rendering (e.g. not using id for a list)
- Bad naming
- Direct DOM manipulation