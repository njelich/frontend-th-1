# frontend-th-1



Welcome to the [Lebesgue](https://lebesgue.io/) take-home task for React developers. Please read the whole task once, and send us questions if you have any. The design sheets are available on the repo, and shown below.

**Your goal is to write a React app that analyzes review sentiment**

![Design sheets for the app](design/all.png)

# Contents

-   [Business need](#business-need)
-   [Use cases](#use-cases)
-   [Evaluation criteria](#evaluation-criteria)
    -   [Technology requirements](#technology-requirements)
    -   [Code requirements](Criteria.md#must-have)
-   [How to submit](#how-to-submit)
-   [How to access API server](#how-to-access-api)
-   [Time limit](#time-limit)

# Business need

The main goal is for users to be able to upload reviews and comments from their websites and filter the ones that need attention.  A great use case for this is identifying relevant comments from social media and other less quantifiable feedback sources.

# Use cases

- The user should be able to:
  - Scroll comments with a show more options
  - Sort and view by sentiment
  - View an overview of general sentiment
  - Paste single or multiple comments into a box or [import a CSV file](#application-input)

The interactions should not refresh the page. Data must persist through reloads (use browser storage).

# Evaluation criteria

## Technology requirements

**React** and **JavaScript** are mandatory requirements. Apart from this, you can use any libraries, task runners and build processors. ES6 and TypeScript are highly encouraged.

## Code requirements

The full criteria for evaluating the coding challenge can be found [here](./Criteria.md).

# How to submit

- Clone this repository.
- A RESTful API for `sentiment` is provided with the challenge. For access, check: [How to access Sentim-API](#how-to-access-api)
- Complete your project as described above within your local repository.
- Make sure that there are scripts to start the client.
- Ensure everything you want to commit is committed before you bundle.
- Create a git bundle: `git bundle create your_name.bundle --all`
- Email the bundle file to us.

**In order to be fair to all candidates, please refrain from sharing your solution on public repository hosting services such as GitHub and Bitbucket until 2021.**

# Application input

The app is meant to take CSV as input, both in the import and in the paste box. The point of the paste box is additional convenience for those importing from sheets and with lesser technical knowledge.

# How to access API

Info about the API is available at https://sentim-api.herokuapp.com

Please use the headers
```
Accept: "application/json", "Content-Type": "application/json"
```
and body
```
{ "text": "Your text here" }
```

As a response you will get a general sentiment structure and a sentence breakdown as below:

```
{
   "result":{
      "polarity":0.53,
      "type":"positive"
   },
   "sentences":[
      {
         "sentence":"This garment is comparable in quality to the leading brands you know and love.",
         "sentiment":{
            "polarity":0.5,
            "type":"positive"
         }
      },
      {
         "sentence":"Meticulously crafted by the same artisans with exceptional construction and craftsmanship.",
         "sentiment":{
            "polarity":0.33,
            "type":"positive"
         }
      },
      {
         "sentence":"The highest quality standards are assured - from the craftsmanship through manufacturing process, to make sure you get the best with every purchase.",
         "sentiment":{
            "polarity":0.75,
            "type":"positive"
         }
      }
   ]
}

```

# Closing remarks

The [design sheet](https://www.figma.com/file/288jWC1BE6hJmgp5KFn7Ny), as well as the criteria are here to help steer you in the right direction. If you see anything additional that could be done - go ahead and do it! The magic sentence is: "How do we make it work?".

We are not setting any hard time limits to this challenge. However, a couple days is enough for the essential function of the app. While we appreciate your effort, we do not want to take up too much of your time. Your main focus should be on making sure [that the application works properly and has some tests](Criteria.md#must-have) before moving on to any secondary objectives. Enjoy!

Good luck,
Noa@Lebesgue